# Parse request params

### Using from file

```
from parse_params import ParseParams
input = """name=Maria, grades-90,age/25?university-UCL,surname=Hill,
grades//85,graduates=5%ID=356231, graduates-7,grades=78//graduates=3"""
pp = ParseParams()
pp.parse(input)
```

### Using from command line

```
python parse_params.py "name=Maria, grades-90,age/25?university-UCL,surname=Hill,grades//85,graduates=5%ID=356231, graduates-7,grades=78//graduates=3"
```

### Requirements

- Python 3

### Notes

Script works only with predefined delimiters:

- ","
- "//"
- "?"
- "%"
- "-"
- "="
- "/"

Script uses predefined Schema:

```
university: "",
students: {
    grades: []
},
graduates: 0
```

Script interprets next params as integer:

- graduates
- grades
- ID
- age
