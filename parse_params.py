"""Library for parsing params from input string with random delimiters."""
import sys


class ParseParams:
    """Main class for parsing params."""

    delimiters = [
        ",",
        "//",
        "?",
        "%",
        "-",
        "=",
        "/"
    ]

    schema = {
        "university": "",
        "students": {
            "grades": []
        },
        "graduates": 0
    }

    int_vals = [
        "graduates",
        "grades",
        "ID",
        "age"
    ]

    def parse(self, str):
        """Parse params from input str string and return results."""
        if len(str) < 2:
            return {}
        result_list = self.create_list(str)
        return self.create_dict(result_list)

    def create_list(self, str):
        """Prepare string and return list with names and values."""
        vals_list = []
        tmp_str = str
        for d in self.delimiters:
            tmp_str = tmp_str.replace(d, " ")
        vals_list = tmp_str.split()
        if len(vals_list) % 2:
            raise Exception(
                "Odd number of values in list '{}'.".format(vals_list)
            )
        return vals_list

    def create_dict(self, result_list):
        """Create dictionary according schema from input list."""
        result = self.schema
        for ind, val in enumerate(result_list):
            if ind % 2:
                continue
            next_val = result_list[ind+1]
            if val in result:
                result[val] = self.parse_val(next_val, val)
            elif val == "grades":
                result["students"]["grades"].append(
                    self.parse_val(next_val, val)
                )
            else:
                result["students"][val] = self.parse_val(next_val, val)
        return result

    def parse_val(self, val, ind):
        """Return gettnig value in properly format."""
        if ind in self.int_vals:
            return int(val)
        return val


if __name__ == "__main__":
    if len(sys.argv) > 1:
        pp = ParseParams()
        print(pp.parse(sys.argv[1]))
