"""Tests for parsing params library."""

import unittest
from parse_params import ParseParams


class ParseParamsTests(unittest.TestCase):
    """Main class for tests."""

    maxDiff = None

    correct_result = {
        "university": "UCL",
        "students": {
            "name": "Maria",
            "surname": "Hill",
            "age": 25,
            "ID": 356231,
            "grades": [90, 85, 78]
        },
        "graduates": 3
    }

    @classmethod
    def setUpClass(cls):
        """Prepare variables for tests."""
        cls._parser = ParseParams()

    def test_standart_input(self):
        """Test case with standart input."""
        input = """name=Maria, grades-90,age/25?university-UCL,surname=Hill,
        grades//85,graduates=5%ID=356231, graduates-7,grades=78//graduates=3"""
        result = self._parser.parse(input)
        self.assertEqual(self.correct_result, result)
